<?php

namespace Lliure\Core\Exception;

use Throwable;

class DataException extends \Exception
{
	
	public function __construct(
		protected array|object $data = [],
		string $message = "",
		int $code = 0,
		string $reasonPhrase = "",
		?Throwable $previous = null
	){
		$base = [];
		
		if(!empty($code)) {
			$base['status_code'] = $code;
		}
		
		if(!empty($reasonPhrase)) {
			$base['reason_phrase'] = $reasonPhrase;
		}
		
		if(!empty($message)) {
			$base['message'] = $message;
		}
		
		foreach($base as $k => $v) (is_array($this->data))? ($this->data[$k] = $v): ($this->data->{$k} = $v);
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * @return array|object
	 */
	public function getData(): array|object
	{
		return $this->data;
	}
	
}